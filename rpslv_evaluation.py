#!/usr/bin/env python3
"""
    A file dedicated to evaluate who the winner of a given pair of inputs of a Rock-Paper-Scissors-Lizard-Spock game is. 
    It can be expanded to extended versions by adding more elements to the figures tuple on the global level
    in this file.
    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""
from __future__ import print_function
from utils import *

"""
GLOBALLY DEFINED VARIABLES
"""

# Feel free to change this!
# FIGURES = ("Scissors", "Paper", "Rock", "Lizard", "Spock")  # caution with order of elements
FIGURES = ("Scissors", "Paper", "Rock")

# Standardized way of printing RPS
if sorted(FIGURES) == ["Paper", "Rock", "Scissors"]:
    CHOICES = "Choices: \n\n" +\
              "  Rock       \n" +\
              "  Paper      \n" +\
              "  Scissors   \n"

# Standardized way of printing RPSLV
elif sorted(FIGURES) == ["Lizard", "Paper", "Rock", "Scissors", "Spock"]:
    CHOICES = "Choices: \n\n" +\
              "  Rock       \n" +\
              "  Paper      \n" +\
              "  Scissors   \n" +\
              "  Lizard     \n" +\
              "  Spock      \n"

# Print from tuple if an unknown game
else:
    CHOICES = "Choices: \n"
    for figs in range(0, len(FIGURES)):
        CHOICES = CHOICES + "\n" + FIGURES[figs]
    CHOICES = "  " + CHOICES + "\n"


def create_matrix():
    """
        Function made to create a matrix of who wins or who loses based on the number of figures in a game.
        Keep in mind the order and number of the figures tuple. It must be an odd number of elements.
        First element wins over second element, last element wins over first element. An element also wins against the 
        element two steps before it, in the case of five elements.
        I haven't researched what relationships exist in higher dimensions, but there will always be an equal amount of
        elements who beat you, as elements you beat. The missing element will be a draw.
    """

    number_of_figures = len(FIGURES)  # expected to be an odd number
    half_set = number_of_figures // 2  # half_set = int((n-1)/2)

    win_and_lose = [-1, +1]  # Negative: row (player_b), Positive: column (player_a), Zero: draw
    
    rows = [[None]*number_of_figures]*number_of_figures # Uses same size in memory as a "full" set  # lint: list


    rows[0] = [0] + win_and_lose*half_set  # [0, -1, +1, -1, +1...]

    for row in range(1, number_of_figures):
        rows[row] = rotate(rows[row-1], 1)

    # for row in range(0, len(rows)):
    #    print(rows[row])
    return FIGURES, rows


def rpslv(input_a, input_b):
    """
        Function made to take two inputs and return who wins. 
    """
    _, winner_matrix = create_matrix()

    # Find row and column of inputs in matrix
    player_a = FIGURES.index(input_a)
    player_b = FIGURES.index(input_b)

    # Check value of item
    value_in_matrix = winner_matrix[player_a][player_b]

    # Set the winner and choice
    if value_in_matrix == -1:
        winner = [1, input_a]
    elif value_in_matrix == +1:
        winner = [2, input_b]
    elif value_in_matrix == 0:
        winner = [0, "Draw"]
    else:
        raise Exception("Unexpected winner")

    return winner


# Demo
if __name__ == "__main__":
    selection_1 = "Paper"
    selection_2 = "Spock"
    find_winner = rpslv(selection_1, selection_2)
    print(selection_1, " vs ", selection_2, ": ", find_winner, sep="")
