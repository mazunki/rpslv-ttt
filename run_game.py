#!/usr/bin/env python3
"""

    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""

# from utils import *
# from rpslv_evaluation import *
from singleplayer import singleplayer
from local_multiplayer import local_multiplayer
from guides import guides
from utils import clear, keystroke, check_version
from tictactoe import *

from time import sleep #temp

current_python_version = None

def run():
    """
    Main menu and function of the program. This is the first called function when run_game.py is executed, 
    and all programs will eventually return here unless program is quit.
    """

    # system("color a")  # hax0rz
    clear()
    
    menu_string = "  Welcome to the game! What would you like to play?   \n" +\
                  "===================================================== \n\n"+\
                  "  1. Singleplayer versus CPU \n" +\
                  "  2. Multiplayer \n" +\
                  "  3. Tic Tac Toe \n" +\
                  "  4. Guides and FAQ \n" +\
                  "  0. Quit \n"

    print(menu_string)

    # Ask for a menu option until selected
    menu_selection = None
    while menu_selection not in [1, 2, 3, 4, 0, "stop"]:
        menu_selection = keystroke()
        if menu_selection not in [1, 2, 3, 4, 0, "stop"]:
            print("Please state a valid number")

    # Singleplayer RPSLV
    if menu_selection == 1:
        return_value = singleplayer()

        while return_value == 1:
            return_value = singleplayer()  # Play again
        else:
            run()  # Returns to main menu

    # Multiplayer RPSLV
    elif menu_selection == 2:
        return_value = local_multiplayer(["",""])

        while return_value[0] in [1, 2]:
            if return_value[0] == 1:
                return_value = local_multiplayer(return_value[1])  # Play again, same players
            elif return_value[0] == 2:
                return_value = local_multiplayer(["", ""])  # Play again, New players
        else:
            run()  # Returns to main menu

    # Tic-Tac-Toe
    elif menu_selection == 3:
        return_value = tictactoe()
        
        while return_value == 1:
            return_value = tictactoe()

        run() 

    # Guides
    elif menu_selection == 4:
        guides()
        run() 

    # Quit
    elif menu_selection in [0, "stop"]:
        print("Goodbye!")
        exit()


# Expected to be executed
if __name__ == '__main__':
    check_version()
    run()
