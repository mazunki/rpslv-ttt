  RPSLV
===============

The game RPSLV is a commonly known game made among nerds by Sam Kass and Karen Bryla.

It's an expansion on the worldwide known Rock Paper Scissors game, including Spock and Lizard as two new hands. 

- Rock smashes Scissors and Lizard.
- Paper covers Rock and disproves Spock.
- Scissors decapitates Lizard and cuts Paper.
- Lizard poisons Spock and eats Paper.
- Spock grabs Scissors and throws Rock.
