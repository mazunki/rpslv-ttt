  Tic Tac Toe:
=================

Tic Tac Toe is a well known game in which two players place their own colours or symbols on a 3 x 3 board in turns on empty spaces aiming to get three successive own figures in line. The first one to get this, wins.
