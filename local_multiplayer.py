#!/usr/bin/env python3
"""
    File containing the necessary multiplayer RPSLV variables.
    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""

from __future__ import print_function

from rpslv_evaluation import *
from utils import clear, keystroke

from getpass import getpass

def local_multiplayer(players=["",""], network=False):
    """
    Function dedicated to a multiplayer RPSLV game, where two users are playing,
    and their choices are hidden until they both have selected their choice.
    Defaults to be playing in the same terminal, with invisible ink.
    """
    clear()
    print("  Welcome to the multiplayer RPSLV game!  \n"+\
          "==========================================\n\n")

    # Set up the players' names, not allowing empty names
    player_name = players
    try:
        while player_name[0] == "":
            player_name[0] = input("  What's the first player's name? ")
        while player_name[1] == "":
            player_name[1] = input("  What's the second player's name? ")
        while player_name[0] == player_name[1]:
            print("The names can't be the same. Please make up some new names.")
            player_name[0] = input("  What's the first player's name? ")
            player_name[1] = input("  What's the second player's name? ")
        getEasterEgg(player_name[0], player_name[1])
    except KeyboardInterrupt:
        player_name[0] = ""
        player_name[1] = ""
        return 0, None
    print()  #\n

    # List the choices from rpslv_evaluation.py
    print(CHOICES)

    # Ask each user for their choice, with invisible ink, until they select a valid choice
    global selection_1
    selection_1 = None
    while selection_1 not in FIGURES:
        print("  Okay ", player_name[0], ", what are you going to play? ", sep="")
        try:
            selection_1 = getpass("").lower().capitalize()
        except KeyboardInterrupt:
            return 0, None

    global selection_2
    selection_2 = None
    while selection_2 not in FIGURES:
        print("  Okay ", player_name[1], ", your turn!", sep="")
        try:
            selection_2 = getpass("").lower().capitalize()
        except KeyboardInterrupt:
            return 0, None
    print()

    # Print each player's individual choice
    print("  ", player_name[0], "chose", selection_1.lower())
    print("  ", player_name[1], "chose", selection_2.lower())

    # Calculate the winner from rpslv_evaluation.py
    who_wins = rpslv(selection_1, selection_2)

    # Print who wins or if it's a draw.
    if not selection_1 == selection_2:
        print("   ", player_name[who_wins[0]-1], " wins with ", who_wins[1].lower(), "!\n", sep="")
    else:
        print("  It's a draw! You both chose", selection_1.lower(), "\n")

    # Menu
    print("1. Play again                  \n" +
          "2. Play again with new players \n" +
          "0. Return to main menu         \n")

    # Ask for a valid option until selected
    menu_selection = None
    while menu_selection not in [1, 2, 0, "stop"]:
        menu_selection = keystroke()
    else:
        print("  Please state a valid number")
    
    if menu_selection == 1:  # Same players
        return 1, player_name
    elif menu_selection == 2:  # New players
        return 2, None
    elif menu_selection == 0:  # Return to main menu
        return 0, None
    elif menu_selection == "stop":
        return 0, None


# In case program is run independently
if __name__ == "__main__":
    return_value = local_multiplayer()
    while return_value[0] in [1,2,0]:
        if return_value[0] == 0:
            print("Nothing to return to! Byeeee!")
            exit()
        if return_value[0] == 1:
            return_value = local_multiplayer(return_value[1])
        if return_value[0] == 2:
            return_value = local_multiplayer(["",""])
        if return_value[0] == 3:
            exit()
