#!/usr/bin/env python3
"""
    A file dedicated to handle the TicTacToe game.
    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""

from __future__ import print_function
from utils import clear, keystroke


def print_grid(real_grid, position, turn):

    # Graphical design is my passion

    circle_art = "      *  *      \n" +\
                 "   *        *   \n" +\
                 "  *          *  \n" +\
                 "  *          *  \n" +\
                 "   *        *   \n" +\
                 "      *  *      \n"

    empty_art = "                \n" +\
                "                \n" +\
                " .............. \n" +\
                "                \n" +\
                "                \n" +\
                "                \n"

    cross_art = "  x        x    \n" +\
                "   x     x      \n" +\
                "      x         \n" +\
                "    x  x        \n" +\
                "  x        x    \n" +\
                "                \n"

    cursor_art = "             x  \n" +\
                 "          x     \n" +\
                 "       x        \n" +\
                 "    x           \n" +\
                 "  x             \n" +\
                 " x              \n"

    graphical_dict = {"O": circle_art, "X": cross_art, "-": empty_art, "/": cursor_art}

    temp_grid = [row[:] for row in real_grid]  # List of lists also uses references with list[:]
    if not position == [None, None]:
        temp_grid[position[1]][position[0]] = "/"


    clear()
    print("  Welcome to Tic Tac Toe!     \n" +
    	  "=========================== \n\n")

    graph_row_a = [""]*6
    graph_row_b = [""]*6
    graph_row_c = [""]*6

    for game_row in range(0,3):
        for graph_row in range(0,6):
            for game_column in range(0,3):
                if game_row == 0:
                    graph_row_a[graph_row] += graphical_dict[temp_grid[game_row][game_column]].split("\n")[graph_row]
                elif game_row == 1:
                    graph_row_b[graph_row] += graphical_dict[temp_grid[game_row][game_column]].split("\n")[graph_row]
                elif game_row == 2:
                    graph_row_c[graph_row] += graphical_dict[temp_grid[game_row][game_column]].split("\n")[graph_row]
            # graph_rows[graph_rows] += "\n"

    graph = [graph_row_a, graph_row_b, graph_row_c]
    for game_row in range(0,len(graph)):
        for graph_row in range(0,6):
            print(graph[game_row][graph_row])

    if turn is not None:
        print("Current turn: ", end="")
        if turn:
            print("X\n")
        else:
            print("O\n")

        print("Use arrow keys to move. Press <Enter> to select your move.")
        print("Press 0 to return to main menu.")

    return 0


def tictactoe():
    clear()
    # print(circle_art, empty_art, cross_art, end="")

    grid = [["-", "-", "-"],
            ["-", "-", "-"],
            ["-", "-", "-"]]

    position = [0, 0]

    turn = False  # Player X
    no_winner_yet = True
    turns_played = 0

    while no_winner_yet and turns_played < 9:
        print_grid(grid, position, turn)

        print()
        # print(position)

        key_hit = keystroke()

        if key_hit in ["left", "right", "up", "down"]:
            if key_hit == "left":
                position[0] = (position[0]-1)%3
            elif key_hit == "right":
                position[0] = (position[0]+1)%3
            elif key_hit == "up":
                position[1] = (position[1]-1)%3
            elif key_hit == "down":
                position[1] = (position[1]+1)%3

        elif key_hit == "enter":  # Select
            if turn:
                if grid[position[1]][position[0]] == "-":
                    grid[position[1]][position[0]] = "X"
                    turn = False
                    turns_played += 1
            else:
                if grid[position[1]][position[0]] == "-":
                    grid[position[1]][position[0]] = "O"
                    turn = True
                    turns_played += 1
        elif key_hit in ["stop", 0]:
            return 0

        winner = None
        # Checking for for winners
        for pair in ["O", "X"]:
            # First in rows
            for row in range(0, len(grid)):
                found_in_row = 0
                for item in range(0, len(grid[row])):
                    if grid[row][item] == pair:
                        found_in_row += 1
                    if found_in_row == 3:
                        winner = pair

            # Then in columns
            for column in range(0, len(grid[0])):
                found_in_column = 0
                for item in range(0, len(grid)):
                    if grid[item][column] == pair:
                        found_in_column += 1
                    if found_in_column == 3:
                        winner = pair

            # And finally in diagonals
            found_in_diagonal = 0
            for diagonal in range(0, len(grid)):
                if grid[diagonal][diagonal] == pair:
                    found_in_diagonal += 1
                if found_in_diagonal == 3:
                    winner = pair

            # And the antidiagonal
            found_in_diagonal = 0
            for diagonal in range(0, len(grid)):
                if grid[diagonal][len(grid)-1-diagonal] == pair:
                    found_in_diagonal += 1
                if found_in_diagonal == 3:
                    winner = pair

        if winner is not None:
            position = [None,None]
            turn = None
            print_grid(grid, position, turn)
            print(winner, "won the game!")
            no_winner_yet = False
    else:
    	if turns_played == 9 and no_winner_yet:
	    	print("Looks like it's a tie! ")

    print()
    print("1. Play again          \n" +
          "0. Return to main menu \n")

    menu_selection = None
    while menu_selection not in [1, 0]:
        menu_selection = keystroke()

        if menu_selection == 1:
            return 1  # Play again
        elif menu_selection == 0 or menu_selection == "stop":
            return 0  # Return to main menu
        else:
            print("Please select a valid option")
