#!/usr/bin/env python3
#coding=utf-8
"""
    A file dedicated to handle the guides menu, importing the necessary files, and printing them.

    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""

from __future__ import print_function
from rpslv_evaluation import create_matrix, FIGURES

from utils import clear, keystroke


def guides():
    """
    A function which will show a menu, allowing the user to pick one of three files, or return to the main menu.
    Each option will show its respective .txt file, and in the case of RPSLV, also show a dynamic table with the rules.
    """

    clear()
    print("  Guides                 \n" +
    	  "=======================\n\n" +
    	  "  1. RPSLV Games         \n" +
          "  2. Tic-Tac-Toe Game    \n" +
          "  3. Author and license  \n" +
          "  0. Return to main menu \n")

    menu_selection = None
    while menu_selection not in [1, 2, 3, 0, "stop"]:  # 1, 2, 3, 0
        menu_selection = keystroke()

    if menu_selection in [1, 2, 3, 0, "stop"]:
        if menu_selection == 1:  # RPSLV Games
            clear()
            with open("guides/rpslv.txt", "r") as rpslv_file:
                rpslv_guide_content = rpslv_file.read()
            print(rpslv_guide_content)

            # A half attempt of making a dynamic table
            print("Check out the diagram below:\n")
            _, rows = create_matrix()

            print("%15s" % "", end="")  # First row only has names
            for column in range(0, len(FIGURES)):
                print("{:^15s}".format(FIGURES[column]), end="")
            print()  # \n

            winner = ""
            for row in range(0, len(FIGURES)):  # All the other rows
                print("{:>15s}".format(FIGURES[row]), end="")  # Starting with the names
                for item in range(0, len(rows)):
                    if rows[row][item] == -1:
                        winner = "←"
                    elif rows[row][item] == 1:
                        winner = "↑"
                    elif rows[row][item] == 0:
                        winner = "0"
                    print("{:^15s}".format(winner), end="")  # All the numbers of this row
                print()  # \n
            print()  # \n

            print("Press any key to return")
            some_key = keystroke()
            if some_key is not "stop":
                guides()
            else:
                return 0

        if menu_selection == 2:  # Tic-Tac-Toe
            clear()
            with open("guides/ttt.txt", "r") as ttt_file:
                ttt_guide_content = ttt_file.read()
            print(ttt_guide_content)

            print("Press any key to return")
            some_key = keystroke()
            if some_key is not "stop":
                guides()
            else:
                return 0

        if menu_selection == 3:  # ToS
            clear()
            with open("guides/tos.txt", "r") as tos_file:
                tos_content = tos_file.read()
            print(tos_content)

            print("Press any key to continue")
            some_key = keystroke()
            if some_key is not "stop":
                guides()
            else:
                return 0

        if menu_selection in ["stop", 0]:
            return 0

    else:
        return 0
