#!/usr/bin/env python3
"""
    Functions that will make the life of a coder easier, and which will also simplify the code, making it easier to
    read. Should be accessible from everywhere.
    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""
from __future__ import print_function

def rotate(input_list, shift=1):
    """
        Function dedicated to rotate the elements of a given list rightwards, adding last elements to the beginning of
        the list.
        Negative shift values will rotate the list leftwards. Works for strings too.
    """

    shift = shift % len(input_list)
    output_list = input_list[-shift:] + input_list[:-shift]
    return output_list


def clear():
    """
    Function to clear the screen depending on the OS being used. I consider importing system could be considered
    a security risk, so I'm not completely happy doing it. If the program was to be used only on a specific terminal
    I could limit it to print only some unicode characters which clear the screen and move the cursor to the top left
    position, but unicode is not supported in Windows.
    """

    from os import system as sys, name as os_name

    if os_name == "nt":
        _ = sys("cls")
    else:
        _ = sys("clear")
        # print(chr(27) + "[2J")

    # return system("cls||clear")
    print()
    return None


def check_version():
    from platform import sys
    global current_python_version

    current_python_version = sys.version_info[0]

def keystroke():
    """
    Function to handle keystrokes in a convient way. It works on Windows as for now.
    """
    from os import name as os_name

    if os_name == "nt":
        from msvcrt import getch

        first_check_input = getch()
        if ord(first_check_input) in [0,224]:
            if ord(first_check_input) == 224:  # Arrow keys, F1-F10 + F13-F22, some system buttons share number
                get_key = ord(getch())  # Second unicode input from buffer is the important button on these
                if get_key == 75:
                    return "left"
                elif get_key == 77:
                    return "right"
                elif get_key == 72:
                    return "up"
                elif get_key == 80:
                    return "down"
        elif ord(first_check_input) == 0:  # Num-pad arrows, F11, F12, F23, F24
                get_key = ord(getch())  # Second unicode input from buffer is the important button on these
                if get_key == 75:
                    print(get_key)
                    return "left"
                elif get_key == 77:
                    return "right"
                elif get_key == 72:
                    return "up"
                elif get_key == 80:
                    return "down"

        elif ord(first_check_input) == 13:
            return "enter"
        elif ord(first_check_input) == 27:
            return "esc"
        elif ord(first_check_input) == 3:
            return "stop"

        else:
            try:
                return int(first_check_input.decode("utf-8"))
            except ValueError:
                return first_check_input.decode("utf-8")

    # The following code is partially imported from Richard Wong with
    # username chao787, at repository getch.py, from https://gist.github.com/chao787/2652257.

    elif os_name in ["linux", "linux2", "posix"]:

	    import tty
	    import sys
	    import termios

	    fd = sys.stdin.fileno()
	    old_settings = termios.tcgetattr(fd)
	    try:
	        tty.setraw(sys.stdin.fileno())
	        ch = sys.stdin.read(1)
	        print(ch)
	        if ch == "\x1b":
	            ch = sys.stdin.read(1)
	            ch = sys.stdin.read(1)
	        print(end="")

	    finally:
	        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	        try:
	            return int(ch)
	        except ValueError:
	            if ch == "A":
	                return "up"
	            elif ch == "B":
	                return "down"
	            elif ch == "C":
	                return "right"
	            elif ch == "D":
	                return "left"
	            elif ch == "\x03":
	                return "stop"
	            elif ch == "\x1b":
	                return "halt"
	            elif ch in ["\r", "\n"]:
	                return "enter"
	            else:
	                return ch

    elif os_name == "carbon":
            import Carbon
            Carbon.Evt

            if Carbon.Evt.EventAvail(0x0008)[0] == 0:  # 0x0008 is the keyDownMask
                return ""
            else:
                (what, msg, when, where, mod) = Carbon.Evt.GetNextEvent(0x0008)[1]
                return msg & 0x000000FF

