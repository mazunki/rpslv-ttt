#!/usr/bin/env python3
"""
    A file dedicated to handle the singleplayer RPSLV menu item.
    
    Author: Rolf Vidar Hoksaas
    Assignment: PR1 Text-Based Games Application
"""

from __future__ import print_function

from rpslv_evaluation import *

from random import randint
from utils import clear, keystroke
from datetime import datetime


def singleplayer(difficulty=2):
    """
    Function dedicated to a singleplayer RPSLV game, with a default difficulty of 2,
    where the CPU chooses at random
    """
    try:
        clear()

        print("  Welcome to the RPSLV game! What would you like to play?   \n"+\
              "============================================================\n")
        print(CHOICES)

        # Ask for a user-input until a valid one is chosen
        selection_one = None
        while selection_one not in FIGURES:
            try:
                selection_one = input("What would you like to play? ").lower().capitalize()
            except KeyboardInterrupt:
                return 0

        # Look for saved games
        try:
            file = open("./data/games.txt", "r")
            if file.mode == "r":
                loaded_games = file.readlines()
                file.close()
        except FileNotFoundError:
            try:
                file = open("./data/games.txt", "x")
                print(" < Creating a new log file. > ")
                file.write("GameData from " + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n\n")
            except FileNotFoundError:
                from os import mkdir
                mkdir("./data")
                file = open("./data/games.txt", "x")
                print(" < Creating a new log file. > ")
                file.write("GameData from " + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n\n")
        except:
            print(" < An error happened loading the save-file. Ignoring. > ")

        # CPU chooses a random hand from the figures
        selection_two = None
        if difficulty == 1:
            selection_two = FIGURES[randint(0, len(FIGURES)-1)]  # Easy
        elif difficulty == 2:
            selection_two = FIGURES[randint(0, len(FIGURES)-1)]  # Normal
        elif difficulty == 3:
            selection_two = FIGURES[randint(0, len(FIGURES)-1)]  # Impossible

        # Prints what the CPU chose
        print("The CPU chose", selection_two.lower(), "\n")

        # Calculate who wins from rpslv_evaluation.py
        who_wins = rpslv(selection_one, selection_two)

        # Print the winner, or draw situation.
        if who_wins[0] == 1:
            print("The player wins against the CPU with ", who_wins[1].lower(), sep="")
            print("Sweet!\n")
        elif who_wins[0] == 2:
            print("The CPU won against the player with ", who_wins[1].lower(), sep="")
            print("Maybe next time?\n")
        elif who_wins[0] == 0:
            print("You both chose the same hand! It's a draw!")
            print("Tiebreak?\n")
        else:
            print("Unexpected winner")

        # Save game to file
        try:
            file
            try:
                file = open("./data/games.txt", "a+")
                if file.mode == "a" or file.mode == "a+":
                    file.write("1={} 2={} w={}\n".format(selection_one, selection_two, who_wins[1]) + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n")
                    file.close()

            except:
                print("< Something unexpected happened while saving the game. Check your data file. >")
        except NameError:
            print(" < Ignoring to save file, preventing possible corruption. >")

        # Prints the menu
        print("1. Play again \n" +
              "0. Return to main menu \n")
      
        # Asks for a valid menu option until selected
        menu_selection = None
        while menu_selection not in [1, 0, "stop"]:
            menu_selection = keystroke()

        if menu_selection == 1:
            return 1
        elif menu_selection == 0:
            try:
                file
                try:
                    file = open("./data/games.txt", "a+")
                    if file.mode == "a" or file.mode == "a+":
                        file.write("<<>>" + "\n\n")
                        file.close()
                except:
                    print("< Something unexpected happened while saving the game. >")
            except NameError:
                print(" < Ignoring to save, preventing possible corruption. > ")

            return 0
        elif menu_selection == "stop":
            return 0

    except KeyboardInterrupt:
        return 0
